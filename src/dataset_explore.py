import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import glob
import seaborn as sns

from src.imports.functions import load_dataset


#%%
bikes = load_dataset('data')

#%% shape of df
bikes.shape

#%% data preprocessing
bikes = bikes.dropna()

#%% add column date, age
bikes['trip datetime'] = pd.to_datetime(bikes['start time'])
bikes['trip date'] = bikes['trip datetime'].dt.date
bikes['age'] = 2017-bikes['birth year']
bikes['minutes'] = bikes['trip duration']/60

#%% maybe drop outliers?
print(bikes.shape)
bikes = bikes[bikes['age'] < np.mean(bikes['age'] )+ 3* np.std(bikes['age'])]
print(bikes.shape)
# there are some too long trips
bikes = bikes[bikes['trip duration'] < np.mean(bikes['trip duration']) + 3* np.std(bikes['trip duration'])]
print(bikes.shape)
#%% zaleznosc dlugosci/czestosci jazdy od pory roku
plt.hist(bikes['trip date'], bins=60)
plt.show()


#%% distplot trip duration men and women
g = sns.distplot(bikes[(bikes['trip duration'] < 5000)]['trip duration'], bins=20, label="Male trip duration")
plt.axvline(np.median(bikes[bikes['trip duration'] < 5000]['trip duration']), color='b', linestyle='--')
plt.legend()
plt.show()

#%% Men
men = bikes[bikes['gender'] == 1]

plt.ylim(0, 1400000)

sns.set_palette("GnBu_d")
# Plotting hist without kde
ax = sns.distplot(men[(men['minutes'] < 80)]['minutes'], kde=False)

# Creating another Y axis
second_ax = ax.twinx()

#Plotting kde without hist on the second Y axis
sns.distplot(men[(men['minutes'] < 80)]['minutes'], kde=True, hist=False,
             axlabel='Minutes', label='Men')

#Removing Y ticks from the second axis
second_ax.set_yticks([])

# Mean line
plt.axvline(np.median(men[men['minutes'] < 80]['minutes']), color='darkslategrey', linestyle='--')

plt.show()



#%% Female
female = bikes[bikes['gender'] == 2]


sns.set_palette(sns.color_palette("husl", 8))

plt.ylim(0, 1400000)

# Plotting hist without kde
ax = sns.distplot(female[(female['minutes'] < 80)]['minutes'], kde=False)

# Creating another Y axis

second_ax = ax.twinx()
plt.ylim(0, 0.25)

#Plotting kde without hist on the second Y axis
sns.distplot(female[(female['minutes'] < 80)]['minutes'], kde=True, hist=False,
             axlabel='Minutes', label='Female')

#Removing Y ticks from the second axis
second_ax.set_yticks([])

# Mean line
plt.axvline(np.median(female[female['minutes'] < 80]['minutes']), color='lightpink', linestyle='--')

plt.show()
#%%
# plt.hist2d(bikes['age'], bikes['trip duration'])
# plt.show()
sns.jointplot(x='age', y='trip duration', data=bikes, kind='hex')
plt.show()

#%%
plt.savefig(os.path.join('figures', 'joint_age_trip_duration.png'))

#%%
