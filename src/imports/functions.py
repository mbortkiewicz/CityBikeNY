import glob
import pandas as pd
import os

def load_dataset(foleder_path):
    """Load bikes csvs files"""
    all_files = glob.glob(os.path.join(foleder_path, "*tripdata.csv"))

    df_from_each_file = (pd.read_csv(f) for f in all_files)

    bikes = pd.concat(df_from_each_file, ignore_index=True)

    bikes = bikes.rename(columns={'tripduration': 'trip duration', 'starttime': 'start time',
                                  'stoptime': 'stop time', 'usertype': 'user type'})
    return bikes
