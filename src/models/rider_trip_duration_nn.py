# Regression Example With Boston Dataset: Baseline

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor
from tensorflow.keras import backend as K


from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.metrics import r2_score
import os
import pandas as pd
import numpy as np
from src.imports.functions import load_dataset

#%%
def coeff_determination(y_true, y_pred):
    SS_res =  K.sum(K.square( y_true-y_pred ))
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )
#%%

# load dataset
bikes = pd.read_csv(os.path.join('data', 'bigger_bikes.csv'))

#%% extract
bikes['minutes'] = bikes['trip duration']/60

#%% calculate speed etc
bikes['miles per hour'] = round(bikes['distance']/bikes['minutes']*60, 2)

#%% drop outliers
print(bikes.shape)
bikes = bikes[(bikes['miles per hour'] < 20) &
              (bikes['miles per hour'] > np.mean(bikes['miles per hour']) - 3*np.std(bikes['miles per hour']))]
bikes = bikes.drop(bikes.index[(bikes['distance'] == 0)])
print(bikes.shape)

#%% delete too long and too short trips --- TODO: think of better cuts
bikes = bikes[(bikes['distance'] > 0.1) &
              (bikes['distance'] < 4.0)]
bikes = bikes[(bikes['trip duration'] > 100) &
              (bikes['trip duration'] < 3200)]
bikes = bikes[(bikes['minutes'] < 45)]

#%% subscriber or customer
bikes['sub or cust'] = [1 if elem == "Subscriber" else 2 for elem in bikes['user type']]

#%% when started trip
bikes['trip datetime'] = pd.to_datetime(bikes['start time'])

bikes['trip start'] = bikes['trip datetime'].dt.time

#%% split into input (X) and output (Y) variables
X = bikes[['gender', 'age', 'sub or cust', 'distance']].values



# y = bikes[['miles per hour']].values
Y = bikes[['trip duration']].values

X_train = X[:1000000, :]
Y_train = Y[:1000000]

X_test = X[1000000:1100000, :]
Y_test = Y[1000000:1100000]
#%%
# define base model
def baseline_model():
    # create model
    model = Sequential()
    model.add(Dense(4, input_dim=4, kernel_initializer='normal', activation='relu'))
    model.add(Dense(1, kernel_initializer='normal'))
    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=[coeff_determination])
    return model

#%% define deeper and wider model
def deeper_model():
    # create model
    model = Sequential()
    model.add(Dense(4, input_dim=4, kernel_initializer='normal', activation='relu'))
    model.add(Dense(200, kernel_initializer='normal', activation='relu'))
    model.add(Dense(20, kernel_initializer='normal', activation='relu'))
    model.add(Dense(1, kernel_initializer='normal'))
    # Compile model
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=[coeff_determination])
    return model

#%% evaluate model
estimator = KerasRegressor(build_fn=baseline_model, epochs=30, batch_size=1024, verbose=1)
kfold = KFold(n_splits=3)
results = cross_val_score(estimator, X_train, Y_train, cv=kfold)
print("Baseline: %.2f (%.2f) MSE" % (results.mean(), results.std()))

#%%
estimator1 = KerasRegressor(build_fn=deeper_model, epochs=10, batch_size=1024, verbose=1)
kfold1 = KFold(n_splits=4)
results1 = cross_val_score(estimator1, X_train, Y_train, cv=kfold1)
print("Deeper: %.2f (%.2f) MSE" % (results1.mean(), results1.std()))



#%%
estimator1.fit(X_train, Y_train)

#%%
predictions = estimator1.predict(X_test)
#%%

from yellowbrick.regressor import PredictionError
from sklearn.linear_model import Lasso
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
model = PredictionError(Ridge(), alpha=0.1, point_color='cornflowerblue')
model.fit(X_train, np.squeeze(Y_train))
model.score(X_test, np.squeeze(Y_test))
model.show()


#%%
from sklearn.base import BaseEstimator, RegressorMixin
class KerasReg(BaseEstimator, RegressorMixin, KerasRegressor):

    def __init__(self, build_fn, **kwargs):
        super(KerasRegressor, self).__init__(build_fn,  **kwargs)
#%%
kf = KerasReg(deeper_model, epochs=10, batch_size=1024, verbose=1)
model = PredictionError(kf, alpha=0.1, point_color='lightpink')
model.fit(X_train, Y_train)
model.score(X_test, Y_test)
model.show()

#%%
from yellowbrick.regressor import ResidualsPlot
visualizer = ResidualsPlot(LinearRegression(), train_alpha=0.1, test_alpha=0.1, train_color='steelblue', test_color='lightblue')

visualizer.fit(bikes.loc[:1000000, ['gender', 'age', 'sub or cust', 'distance']], bikes.loc[:1000000, 'trip duration'])  # Fit the training data to the visualizer
# visualizer.fit(X_train, Y_train)

visualizer.score(X_test, np.squeeze(Y_test))  # Evaluate the model on the test data
visualizer.show()


#%%nn
from yellowbrick.regressor import ResidualsPlot
visualizer = ResidualsPlot(kf, train_alpha=0.1, test_alpha=0.1, train_color='palevioletred', test_color='lightpink')

# visualizer.fit(bikes.loc[:1000000, ['gender', 'age', 'sub or cust', 'distance']], bikes.loc[:1000000, 'trip duration'])  # Fit the training data to the visualizer
visualizer.fit(X_train, Y_train)

visualizer.score(X_test, np.squeeze(Y_test))  # Evaluate the model on the test data
visualizer.show()