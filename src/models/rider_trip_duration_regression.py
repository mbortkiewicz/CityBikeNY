import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
import pandas as pd
import os
import matplotlib.pyplot as plt

bikes = pd.read_csv(os.path.join('data', 'bigger_bikes.csv'))

#%% extract
bikes['minutes'] = bikes['trip duration']/60

#%% calculate speed etc
bikes['miles per hour'] = round(bikes['distance']/bikes['minutes']*60, 2)

#%% drop outliers
print(bikes.shape)
bikes = bikes[(bikes['miles per hour'] < 20) &
              (bikes['miles per hour'] > np.mean(bikes['miles per hour']) - 3*np.std(bikes['miles per hour']))]
bikes = bikes.drop(bikes.index[(bikes['distance'] == 0)])
print(bikes.shape)

#%% delete too long and too short trips --- TODO: think of better cuts
bikes = bikes[(bikes['distance'] > 0.1) &
              (bikes['distance'] < 4.0)]
bikes = bikes[(bikes['trip duration'] > 100) &
              (bikes['trip duration'] < 3200)]
bikes = bikes[(bikes['minutes'] < 45)]

#%% subscriber or customer
bikes['sub or cust'] = [1 if elem == "Subscriber" else 2 for elem in bikes['user type']]

#%% when started trip
bikes['trip datetime'] = pd.to_datetime(bikes['start time'])

bikes['trip start'] = bikes['trip datetime'].dt.time

#%% reg model
X = bikes[['gender', 'age', 'sub or cust', 'distance']].values



# y = bikes[['miles per hour']].values
y = bikes[['trip duration']].values


#%%
X = X[:1000000,:]
y = y[:1000000]
reg = LinearRegression(n_jobs=-1, normalize=True).fit(X, y)

print(reg.score(X, y))

print(reg.coef_)

print(reg.predict(np.array([[2, 48, 1, 1.67745]])))
# print(reg.get_params())

#%%

