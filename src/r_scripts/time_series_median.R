library(data.table)
library(dplyr)
library(ggplot2)
# install.packages("tidyr")
library(tidyr)
library(plyr)
library(plotly)

library(ggplot2)
library(ggmap)
library(mapproj)

library(gganimate)


filenames <- list.files("data", pattern="*.csv", full.names=TRUE)


df1 <- rbindlist(lapply(filenames[13:23],fread))
data_prep <- na.omit(df1)


data_prep$sub <- ifelse(data_prep$usertype == "Subscriber", 1, 0)
data_prep$cust <- ifelse(data_prep$usertype == "Customer", 1, 0)

# data_prep_small <- data_prep[sample(nrow(data_prep), 150000), ]

data_prep_small <- data_prep[seq(1, 1200000), ]
data_prep_small <- data_prep
# data_prep_small$hour <- as.POSIXlt(data_prep_small$starttime)$hour
data_prep_small$day <- format(as.Date(data_prep_small$starttime, format="%Y-%m-%d"), format = "%d")
data_prep_small$month <- format(as.Date(data_prep_small$starttime, format="%Y-%m-%d"), format = "%m")
data_prep_small$year <- format(as.Date(data_prep_small$starttime, format="%Y-%m-%d"), format = "%Y")
data_prep_small$day_of_year <-format(as.Date(data_prep_small$starttime, format="%Y-%m-%d"), format = "%j")
data_prep_small$age <- 2017 - as.numeric(data_prep_small$`birth year`) 

data_prep_small$sub_cust <- ifelse(data_prep$usertype == "Subscriber", 1, 0)

data_prep_users <- data_prep_small[, c(16, 17, 18, 19, 20, 21, 22)]



data_prep_users_group <- data_prep_small %>% 
  group_by(year, day_of_year, gender) %>%
  summarise(median_trip = median(tripduration))

data_prep_users_group$day_of_year <- as.numeric(data_prep_users_group$day_of_year)


new <- na.omit(data_prep_users_group)
new$plec <- factor(paste(ifelse(new$gender==0, "Niewiadomo", ifelse(new$gender==1, "Mezczyzna", "Kobieta"))))

p <- ggplot(
  new,
  aes(day_of_year, median_trip, group = plec, color = plec)
) +
  geom_line() +
  geom_smooth(se = FALSE, method = "loess") +
  # scale_color_viridis_d() +
  # scale_fill_manual(palette = "Dark2") +
  labs(x = "Dzien roku 2019", y = "Mediana czasu jazdy") +
  theme(legend.position = "top") 

p
p + transition_reveal(day_of_year)


ggsave("plot.png", width = 10, height = 10)
