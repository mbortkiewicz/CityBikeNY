import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import glob
import seaborn as sns
import geopy.distance

from src.imports.functions import load_dataset

bikes = load_dataset('data')
bikes = bikes.dropna()

#%%
bikes['age'] = 2017-bikes['birth year']
bikes['age'] = bikes['age'].astype(int)

#%%
bikes['start coordinates'] = list(zip(bikes['start station latitude'], bikes['start station longitude']))
bikes['end coordinates'] = list(zip(bikes['end station latitude'], bikes['end station longitude']))

#%% maybe drop outliers?
print(bikes.shape)
bikes = bikes[bikes['age'] < np.mean(bikes['age'] )+ 3* np.std(bikes['age'])]
print(bikes.shape)
# there are some too long trips
bikes = bikes[bikes['trip duration'] < np.mean(bikes['trip duration']) + 3* np.std(bikes['trip duration'])]
print(bikes.shape)
# there are some trips to same station
bikes = bikes[bikes['start station id'] != bikes['end station id']]
print(bikes.shape)

#%% create dataset for model
dist = []
for i in range(len(bikes)):
# for i in range(10000000):
    dist.append(geopy.distance.geodesic(bikes.iloc[i]['start coordinates'], bikes.iloc[i]['end coordinates']).miles)
    if (i % 100000 == 0):
        print(i)

smaller_bikes = bikes[['gender', 'age', 'user type', 'trip duration', 'start time']]
# smaller_bikes = smaller_bikes[:10000000]
smaller_bikes['distance'] = dist
# smaller_bikes.reset_index(inplace=True)

smaller_bikes = smaller_bikes[['gender', 'age', 'user type', 'trip duration', 'start time', 'distance']]

smaller_bikes.to_csv(os.path.join('data', 'bigger_bikes.csv'), index=False)
