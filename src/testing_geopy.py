from geopy import distance
newport_ri = (41.49008, -71.312796)
cleveland_oh = (41.499498, -81.695391)
print(distance.geodesic(newport_ri, cleveland_oh).miles)
print(distance.geodesic(newport_ri, cleveland_oh))
print(distance.geodesic(newport_ri, cleveland_oh).km)

wellington = (-41.32, 174.81)
salamanca = (40.96, -5.50)
print(distance.geodesic(wellington, salamanca).km)
