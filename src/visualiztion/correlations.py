import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os

#%% load dataset
bikes = pd.read_csv(os.path.join('data', 'bigger_bikes.csv'))

#%% extract
bikes['minutes'] = bikes['trip duration']/60

#%% calculate speed etc
bikes['miles per hour'] = round(bikes['distance']/bikes['minutes']*60, 2)

#%% drop outliers
print(bikes.shape)
bikes = bikes[(bikes['miles per hour'] < 20) &
              (bikes['miles per hour'] > np.mean(bikes['miles per hour']) - 3*np.std(bikes['miles per hour']))]
bikes = bikes.drop(bikes.index[(bikes['distance'] == 0)])
print(bikes.shape)

#%% delete too long and too short trips --- TODO: think of better cuts
bikes = bikes[(bikes['distance'] > 0.1) &
              (bikes['distance'] < 4.0)]
bikes = bikes[(bikes['trip duration'] > 100) &
              (bikes['trip duration'] < 3200)]
bikes = bikes[(bikes['minutes'] < 45)]

#%%
mini_bikes = bikes.loc[:, ['age', 'trip duration', 'distance']]

#%%
corr = mini_bikes.corr()

mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
plt.show()


#%%
bikes.groupby(['age']).mean()['miles per hour'].plot(legend=True)
plt.show()


#%%
sns.set(style="ticks")

# Initialize the figure with a logarithmic x axis
f, ax = plt.subplots(figsize=(12, 6))
# ax.set_xscale("log")

# Load the example planets dataset

# Plot the orbital period with horizontal boxes
sns.boxplot(x="age", y="miles per hour", data=bikes,
            whis="range", palette="vlag")
# ax.set_xticks([20, 30, 40, 50, 60, 70])
ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha="right")
plt.tight_layout()




for label in ax.xaxis.get_ticklabels()[::2]:
    label.set_visible(False)
# for label in ax.xaxis.get_ticklabels()[4::5]:
#     label.set_visible(True)

sns.swarmplot(x="age", y="miles per hour", data=bikes.iloc[::1000, :],
              size=1.5, color=".3", linewidth=0)

#%%
