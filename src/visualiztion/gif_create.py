import pandas as pd
import numpy as np
from pandas.api.types import CategoricalDtype
from plotnine import *
from src.imports.functions import load_dataset
#%%
bikes = load_dataset('data')

#%% shape of df
bikes.shape

#%% data preprocessing
bikes = bikes.dropna()

#%% add column date, age
bikes['trip datetime'] = pd.to_datetime(bikes['start time'])
bikes['trip date'] = bikes['trip datetime'].dt.date
bikes['age'] = 2017-bikes['birth year']
bikes['minutes'] = bikes['trip duration']/60
bikes['hour'] = pd.to_datetime(bikes['start time']).dt.hour

#%%
bikes2 = bikes.iloc[:1000000, :]

#%%
bike_group = bikes2.groupby(['hour', 'gender'], as_index=False).mean()
bike_group['gender'] = bike_group['gender'].astype(str)
#%%
bike_group1 = bike_group.loc[:, ['hour', 'gender', 'age', 'trip duration']]

bike_group1['minuty'] = bike_group1['trip duration']/60

#%%
for i in range(0, 24):

    (ggplot(bike_group1.loc[bike_group1['hour']==i])
     + aes(x='age', y='minuty', color='gender', size=10)
     + geom_point()
     + labs(title='Godzina '+str(i) + ':00', x='Wiek', y='Czas podrozy (minuty)')
     + xlim(32, 48)
     + ylim(400/60, 1100/60)
    ).save('figures/gif/' + str(i) + ".png")


