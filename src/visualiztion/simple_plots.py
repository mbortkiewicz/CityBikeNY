import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import seaborn as sns

from src.imports.functions import load_dataset

#%%
def simple_displot(data, feature, x_limit, y_limit, gender, axlabel, scale_factor=1):
    if gender == 1:
        sns.set_palette("GnBu_d")
        color = 'darkslategrey'
        label = 'Men'
    else:
        sns.set_palette(sns.color_palette("husl", 8))
        color = 'lightpink'
        label = 'Female'

    plt.ylim(y_limit)

    # Plotting hist without kde
    ax = sns.distplot(data[(data[feature] < x_limit[1])][feature], kde=False)

    # Creating another Y axis
    second_ax = ax.twinx()

    if gender == 2:
        plt.ylim(0, scale_factor)

    # Plotting kde without hist on the second Y axis
    sns.distplot(data[(data[feature] < x_limit[1])][feature], kde=True, hist=False,
                 axlabel=axlabel, label=label)

    # Removing Y ticks from the second axis
    second_ax.set_yticks([])

    # Mean line
    plt.axvline(np.median(data[(data[feature] < x_limit[1])][feature]), color=color, linestyle='--')

    plt.show()

#%%
bikes = load_dataset('data')
bikes = bikes.dropna()
bikes['age'] = 2017-bikes['birth year']

#%%
men = bikes[bikes['gender'] == 1]
women = bikes[bikes['gender'] == 2]

#%%

simple_displot(men, 'age', [0, 80], [0, 850000], 1, 'age')
simple_displot(women, 'age', [0, 80], [0, 850000], 2, 'age', (1/7))

#%% gender
plt.style.use('seaborn-deep')

bikes['gender'].hist()
plt.show()

#%% station
bikes['start station id'].hist()
plt.show()

#%% most popular trips
trips_df = bikes.groupby(['start station name', 'end station name']).size().reset_index(name='number of trips')\
    .sort_values('number of trips', ascending=False)
trips_df['start end'] = trips_df['start station name'] + '\n' + trips_df['end station name']


plt.figure(figsize=(10,5))
chart = sns.barplot(x='start end', y='number of trips', data=trips_df.head(5), palette=sns.cubehelix_palette(8))
chart.set_xticklabels(chart.get_xticklabels(),
                      rotation=45,
                      horizontalalignment='right',
                      fontweight='light',
                      fontsize='large')
plt.show()
#%% most popular start station
start_end_df = bikes.groupby(['start station name']).size().reset_index(name='number of starts')\
    .sort_values('number of starts', ascending=False)

plt.figure(figsize=(10,5))
chart = sns.barplot(x='start station name', y='number of starts', data=start_end_df.head(5), palette="Blues_d")
chart.set_xticklabels(chart.get_xticklabels(),
                      rotation=45,
                      horizontalalignment='right',
                      fontweight='light',
                      fontsize='large')
plt.show()


#%% most popular end station
start_end_df = bikes.groupby(['end station name']).size().reset_index(name='number of ends')\
    .sort_values('number of ends', ascending=False)

plt.figure(figsize=(10,5))
chart = sns.barplot(x='end station name', y='number of ends', data=start_end_df.head(5), palette="Blues_d")
chart.set_xticklabels(chart.get_xticklabels(),
                      rotation=45,
                      horizontalalignment='right',
                      fontweight='light',
                      fontsize='large')
plt.show()


#%%
start_end_df = bikes.groupby(['bikeid']).size().reset_index(name='number of trips')\
    .sort_values('number of trips', ascending=False)

# plt.figure(figsize=(10,5))
chart = sns.barplot(x='bikeid', y='number of trips', data=start_end_df.head(5), palette=sns.cubehelix_palette(8), order=[25738,  27161,25275, 26565,27117])
# chart.set_xticklabels(chart.get_xticklabels(),
#                       rotation=45,
#                       horizontalalignment='right',
#                       fontweight='light',
#                       fontsize='large')
plt.show()

#%%
ax = sns.distplot(start_end_df['number of trips'], kde=False,
             axlabel='number of trips')

second_ax = ax.twinx()

sns.distplot(start_end_df['number of trips'], kde=True, hist=False,
             axlabel='number of trips', )

#Removing Y ticks from the second axis
second_ax.set_yticks([])

# Mean line
plt.axvline(np.median(start_end_df['number of trips']), color='steelblue', linestyle='--')
plt.show()

